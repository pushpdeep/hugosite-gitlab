var compare = "Pheretima posthuma";
var target2;
var isItalic = 0, isBold = 0, isUl = 0;
//Getting all the divs
var div1 = document.getElementById('divFirst');
var div2 = document.getElementById('divSecond');
var div3 = document.getElementById('divFinal');
var divQue = document.getElementById('que');
var divStart = document.getElementById('start');
var divEnd = document.getElementById('end');
var divLast = document.getElementById('lastDiv');

//Pointers for the eartworm images.
var imgreadptr = document.getElementById('pointer3');
var imgpointptr = document.getElementById('pointer4');
var imgupptr = document.getElementById('pointer2');
var imgdwnptr = document.getElementById('pointer1');

//Pointers for all the tags
var kingdom = document.getElementById('tag1');
var help = document.getElementById('tag2');
var drgdrp = document.getElementById('tag3');
var classify = document.getElementById('tag4');
var salftrs = document.getElementById('tag5');
var dblclicksalftrs = document.getElementById('tag6');
var drgdrpalftrs = document.getElementById('tag7');
var zlname = document.getElementById('tag8');
var performance = document.getElementById('tag9');

//Help buttons
var help_btn1 = document.getElementById('help_btns');
var help_btn2 = document.getElementById('help_btns2');
var help_btn3 = document.getElementById('help_btns3');

function boldText() {
    var target = document.getElementById("TextArea");
    if (target.style.fontWeight == "bold") {
        target.style.fontWeight = "normal";
        isBold = 0;
    } else {
        target.style.fontWeight = "bold";
        isBold = 1;
    }
}

function italicText() {
    var target = document.getElementById("TextArea");
    if (target.style.fontStyle == "italic") {
        target.style.fontStyle = "normal";
        isItalic = 0;
    } else {
        target.style.fontStyle = "italic";
        isItalic = 1;
    }
}

function underlineText() {
    var target = document.getElementById("TextArea");
    if (target.style.textDecoration == "underline") {
        target.style.textDecoration = "none";
        isUl = 0;
    } else {
        target.style.textDecoration = "underline";
        isUl = 1;
    }
}

//To hide all the divs.
function hideAllDivs() {
    div1.style.display = "none";
    div2.style.display = "none";
    div3.style.display = "none";
    divQue.style.display = "none";
    divStart.style.display = "none";
    divEnd.style.display = "none";
    divLast.style.display = "none";
}

function hideAllTags() {
    kingdom.style.display = "none";
    help.style.display = "none";
    drgdrp.style.display = "none";
    classify.style.display = "none";
    salftrs.style.display = "none";
    dblclicksalftrs.style.display = "none";
    drgdrpalftrs.style.display = "none";
    zlname.style.display = "none";
    performance.style.display = "none";
}

function hideAllGifs() {
    //Pointers for the eartworm images.
    imgreadptr.style.display = "none";
    imgpointptr.style.display = "none";
    imgupptr.style.display = "none";
    imgdwnptr.style.display = "none";
    imgdwnptr.style.display = "none";
}
