---
title: Procedure
subtitle: Follow the procedure
comments: false
---

Procedure:
1)	Student has to click start button<br>
2)	Student has to drag and drop the circles according to correct taxonomical order and click submit.<br> 
3)	There will be 4 chance to do it. At 5th wrong attempt student can click yes option to take help or No to continue.<br> 
4)	Student has to type specific terminology used for classification of earthworm and click submit.<br> 
5)	There will be 4 chance to do it. At 5th wrong attempt student can click yes option to take help or No to continue.<br> 
6)	Student can to double click on characters in the box of salient features and read the description.<br> 
7)	Student can close the popup and go for new character and repeat the same procedure.<br>
8)	Student has to drag and drop the salient feature in empty boxes to match with appropriate character of a particular taxonomical category and click submit.<br>
9)	Student has to type Zoological name of earthworm in box,  make it italic and submit.<br>
10)	Student has to click appropriate answers of assessment questions and submit.<br>
11)	At last student will get result and performance indicator.<br>
