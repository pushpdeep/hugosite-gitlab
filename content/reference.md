---
title: References
subtitle: refer below docs if more details required
comments: false
---

References

Invertebrate Zoology by R.L Kotpal paper 2009 <br>
Principles and Practices of Animal Taxonomy by V.C Kappor <br>
Text Book of Zoology by Dr. Arbale, Phadake publication.
