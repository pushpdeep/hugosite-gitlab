---
title: Post-Test
subtitle: what did you learn from the simulator ?
comments: false
---


Post Test qiestions 
1. Which of the following is correct taxonomical order for  classification of animals?<b> **a)Kingdom-Phylum-Class-Order-Genus-Species** <b>b)Kingdom- Class - Phylum -Order-Genus-Species<b> c)Kingdom-Phylum-Class- Genus - Order -Species<b>d)Kingdom-Phylum- Genus -Order- Class �Species
2. Which of the following taxonomical category specifically tells about the individual organism.<br> a)Kingdom <br>b) Class <br>c)Phylum <br>**d)Species**
3. To which Phylum does the earthworm belongs? <b>a)Arthropoda <b> **b)Annelida** <b>c)Echinodermata <b>d)Coelenterate  
4. Which  is the most  important feature of phylum annelid? <b> **a)Body is  metamerically segmented** <b>b)Presence of prostomium <b>c)Complete digestive system <b>d) Closed circulatory system.
5. Earthworm is categorized  in class oligochaeta  due to ........ <b> a)large number of nephridio pores<b>  **b)few number of setae** <b>c) presence of prostomium <b> d)Locomotary organs are absent.
6. Identify the correct zoological name of earthworm? <b>a)Pheritima posthuma <b> **b)** __Pheritima posthuma__ Ans V<b> c) Pheritima Posthuma <b>d) posthuma Pheritima
7. Which of the following taxonomical category specifically tells about the individual organism.<b> a)Kingdom  <b> b) Class <b> c)Phylum <b> **d)Species**
8. Which of the following is correct about clitellum  <b> a)It is made of non glandular cells <b> b) It liberates gametes <b> **c) It is made of glandular, secretory cells** <b> d)It is exactly present in middle part of body <b>
9. Which of the following is NOT correct about prostomium <b> a)It is a fleshy lobe  <b> b)It is not a complete circle <b> **c)It posses setae** <b> d)It covers opening of mouth
10.Germinal Zone is present at....... <b> a)Prostomium 	<b> b)Clitellum <b>c) Middle metameres  <b>**d)Pygidium**<br>
