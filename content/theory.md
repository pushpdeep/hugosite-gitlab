---
title: Theory
subtitle: Detailed description about the experiment
comments: false
---


Theory
    Classification of animals is governed by some set of rules. These rules are derived from principles of taxonomy and general theory of systemic. Taxonomy is based on evolutionary account of organism coming from broadest category to smallest unit that is individual organism. It provides a systematic hierarchy from kingdom to phylum, phylum to class, class to order, order to genus, genus to species. The animals are categorized  systematically based on morphological and physiological characters. Morphological and physiological characters used to group organism and the group is given a specific nomenclature and then this group is assigned to particular level of taxonomical order. In this way classification is completed and systemic position of given animal is achieved. Therefore each taxonomical category has specific name for particular group of animal which is justified by giving reasons to include a animal in that group.  
